package com.example.learningapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.learningapp.R
import com.example.learningapp.models.Film

class ProfileListAdapter(val context : Context, val films : List<Film>) : RecyclerView.Adapter<ProfileListAdapter.Link>() {
    class Link(itemView : View) : RecyclerView.ViewHolder(itemView) {
//        val title : TextView = itemView.findViewById(R.id.textViewProfileDescriptionTitle)
        val description : TextView = itemView.findViewById(R.id.textViewProfileDescription)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Link {
        val root = LayoutInflater.from(context).inflate(R.layout.profile_description_item, parent, false)
        return Link(root)
    }

    override fun getItemCount(): Int {
        return films.size
    }

    override fun onBindViewHolder(holder: Link, position: Int) {
//        holder.title.text = films[position].title
        holder.description.text = films[position].opening_crawl
    }
}