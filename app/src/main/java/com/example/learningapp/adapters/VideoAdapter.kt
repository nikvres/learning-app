package com.example.learningapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.learningapp.R
import com.example.learningapp.models.Video
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView


class VideoAdapter(val context : Context, val videos : List<Video>) : RecyclerView.Adapter<VideoAdapter.Link>() {

    class Link(itemView : View) : RecyclerView.ViewHolder(itemView) {
//        val title : TextView = itemView.findViewById(R.id.textViewVideoTitle)
        val video : YouTubePlayerView = itemView.findViewById(R.id.video_player)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Link {
        val root = LayoutInflater.from(context).inflate(R.layout.video_item, parent, false)
        return Link(root)
    }

    override fun getItemCount(): Int {
        return videos.size
    }

    override fun onBindViewHolder(holder: Link, position: Int) {
//        holder.title.text = videos[position].name
        val pos = position;
        holder.video.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {

            override fun onReady(youTubePlayer: YouTubePlayer) {
                super.onReady(youTubePlayer)
                val videoID = videos[pos].url
                youTubePlayer.loadVideo(videoID, 0f)
            }

        })
    }
}