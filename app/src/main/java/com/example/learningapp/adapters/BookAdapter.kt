package com.example.learningapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.learningapp.R
import com.example.learningapp.models.Book

class BookAdapter(val context : Context, val books : List<Book>) : RecyclerView.Adapter<BookAdapter.Link>() {
    class Link(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val title : TextView = itemView.findViewById(R.id.bookItem_TitleTextView)
        val description : TextView = itemView.findViewById(R.id.bookItem_DescriptionTextView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Link {
        val root = LayoutInflater.from(context).inflate(R.layout.book_item, parent, false)
        return Link(root)
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: Link, position: Int) {
        holder.title.text = books[position].title
        holder.description.text = books[position].description
    }
}
