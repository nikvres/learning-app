package com.example.learningapp.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AppAPI {
    fun getNewFilmAPI()=Retrofit.Builder().baseUrl("https://swapi.dev/api/").addConverterFactory(GsonConverterFactory.create()).build()
    fun getNewBooksAPI()=Retrofit.Builder().baseUrl("https://harry-potter-api-en.onrender.com/").addConverterFactory(GsonConverterFactory.create()).build()
}