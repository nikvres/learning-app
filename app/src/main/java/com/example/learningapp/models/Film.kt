package com.example.learningapp.models
data class FilmsData(val results : List<Film>)
data class Film(val title : String, val opening_crawl : String)
