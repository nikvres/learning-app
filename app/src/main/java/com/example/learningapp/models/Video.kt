package com.example.learningapp.models

data class Video(val name : String, val url : String)
