package com.example.learningapp.models

data class Book (val title : String, val description : String)
