package com.example.learningapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.learningapp.R
import com.example.learningapp.activities.USER_TABLE
import com.example.learningapp.adapters.ProfileListAdapter
import com.example.learningapp.api.AppAPI
import com.example.learningapp.interfaces.IFilm
import com.example.learningapp.models.FilmsData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment : Fragment() {
    var sharedPreferences: SharedPreferences? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var retrofit = AppAPI().getNewFilmAPI()
        var filmAPI = retrofit.create(IFilm::class.java)

        val context = inflater.inflate(R.layout.fragment_profile, container, false)
        val profileImage: ImageView = context.findViewById(R.id.profileFragment_ImageViewPhoto)
        val profileName: TextView = context.findViewById(R.id.profileFragment_TextViewGreetings)

        val recyclerDescription: RecyclerView =
            context.findViewById(R.id.profileFragment_RecyclerViewDescription)

        sharedPreferences = activity?.getSharedPreferences(USER_TABLE, Context.MODE_PRIVATE)

        profileName.text = "C возвращением,\n${sharedPreferences?.getString("name", "Пользователь")}"
        filmAPI.GetAllFIlms().enqueue(object : Callback<FilmsData> {

            override fun onResponse(call: Call<FilmsData>, response: Response<FilmsData>) {
                val filmsList = response.body().let { it!!.results }
                recyclerDescription.adapter = ProfileListAdapter(activity!!.applicationContext, filmsList)
            }

            override fun onFailure(call: Call<FilmsData>, t: Throwable) {
            }

        })

        Glide
            .with(context)
            .load("https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/b0410a121228813.60c165246b6ca.jpg")
            .into(profileImage);

        return context
    }

}