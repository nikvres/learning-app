package com.example.learningapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.learningapp.R
import com.example.learningapp.activities.USER_TABLE
import com.example.learningapp.adapters.VideoAdapter
import com.example.learningapp.models.Video

class VideosFragment : Fragment() {


    val videosObj = arrayListOf(
        Video("Первое видео", "bD7bpG-zDJQ"), Video("Второе видео", "gYbW1F_c9eM-zDJQ"),
        Video("Третье видео", "opfJIo__ANQ")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val context = inflater.inflate(R.layout.fragment_videos, container, false)

        val recyclerView : RecyclerView = context.findViewById(R.id.videosFragment_recyclerView)
        recyclerView.adapter = VideoAdapter(requireActivity().applicationContext, videosObj)

        return context
    }

}