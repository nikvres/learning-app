package com.example.learningapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.example.learningapp.R
import com.example.learningapp.activities.USER_TABLE
import com.example.learningapp.adapters.FavouriteAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class FavouriteFragment : Fragment() {

    var sharedPreferences: SharedPreferences? = null

    private val fragmentList = listOf(VideosFragment(), BooksFragment())
    private val tabLayoutNames = listOf("Смотреть", "Читать")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sharedPreferences = activity?.getSharedPreferences(USER_TABLE, Context.MODE_PRIVATE)

        val context = inflater.inflate(R.layout.fragment_favourite, container, false)
        val username : TextView = context.findViewById(R.id.favouriteFragment_textViewUsername)
        val tabLayout : TabLayout = context.findViewById(R.id.tabLayout)
        val viewPager : ViewPager2 = context.findViewById(R.id.fragment_holder)

        username.text = sharedPreferences?.getString("name", "Пользователь") + ","
        viewPager.adapter = FavouriteAdapter(this, fragmentList)

        TabLayoutMediator(tabLayout, viewPager) {
                tab,pos -> tab.text = tabLayoutNames[pos]
        }.attach()

        return context
    }

}