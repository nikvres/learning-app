package com.example.learningapp.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.learningapp.R
import com.example.learningapp.activities.USER_TABLE

class SettingsFragment : Fragment() {

    var sharedPreferences: SharedPreferences? = null

    lateinit var userNameEditText: EditText
    lateinit var userEmailEditText: EditText
    lateinit var userPasswordEditText: EditText
    lateinit var userPasswordAgainEditText: EditText
    lateinit var usernameTextView: TextView
    lateinit var buttonSave: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val context = inflater.inflate(R.layout.fragment_settings, container, false)
        userNameEditText = context.findViewById(R.id.settingsFragment_editTextName)
        userEmailEditText = context.findViewById(R.id.settingsFragment_editTextEmail)
        userPasswordEditText = context.findViewById(R.id.settingsFragment_editTextPassword)
        userPasswordAgainEditText =
            context.findViewById(R.id.settingsFragment_editTextPasswordAgain)
        usernameTextView = context.findViewById(R.id.settingsFragment_textViewUsername)
        sharedPreferences = activity?.getSharedPreferences(USER_TABLE, Context.MODE_PRIVATE)
        buttonSave = context.findViewById(R.id.settingsFragment_saveButton)
        buttonSave.setOnClickListener {

            if (userNameEditText.text.toString().isEmpty()
                || userEmailEditText.text.toString().isEmpty()
                || userPasswordEditText.text.toString().isEmpty()
            ) {
                Toast.makeText(
                    activity?.applicationContext,
                    "Заполните все поля",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            if (!checkEmailPattern(userEmailEditText.text.toString())) {
                Toast.makeText(
                    activity?.applicationContext,
                    "Введите валидную почту",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            if (userNameEditText.text.toString() != sharedPreferences?.getString("name", "")
                || userEmailEditText.text.toString() != sharedPreferences?.getString("email", "") ||
                sharedPreferences?.getString(
                    "password",
                    ""
                ) != userPasswordEditText.text.toString()
            ) {
                if (userPasswordEditText.text.toString() != userPasswordAgainEditText.text.toString()) {
                    Toast.makeText(
                        activity?.applicationContext,
                        "Введите пароль еще раз",
                        Toast.LENGTH_SHORT
                    ).show()
                    return@setOnClickListener
                }

                addUserToSharedPreferences(
                    userEmailEditText.text.toString(),
                    userPasswordAgainEditText.text.toString(),
                    userNameEditText.text.toString()
                )

                Toast.makeText(
                    activity?.applicationContext,
                    "Данные были успешно изменены",
                    Toast.LENGTH_SHORT
                ).show()
                userPasswordAgainEditText.setText("")
                sharedPreferences?.edit()?.putBoolean("isLogin", false)?.apply()
            } else {
                Toast.makeText(
                    activity?.applicationContext,
                    "Данные не были изменены",
                    Toast.LENGTH_SHORT
                ).show()
            }

            updateUserData()

        }
        updateUserData()
        return context
    }

    fun checkEmailPattern(emailString: String): Boolean {
        val emailPattern = "^[a-zA-Z0-9]{1,50}@[a-z]{1,10}\\.[a-z]{2,4}$".toRegex()
        return emailPattern.matches(emailString)
    }

    fun addUserToSharedPreferences(email: String, password: String, name: String) {
        val editor = sharedPreferences?.edit()
        editor?.putString("email", email)
        editor?.putString("password", password)
        editor?.putString("name", name)
        editor?.apply()
    }

    fun updateUserData() {
        usernameTextView.setText(sharedPreferences?.getString("name", "Пользователь") + ",")
        userNameEditText.setText(sharedPreferences?.getString("name", ""))
        userEmailEditText.setText(sharedPreferences?.getString("email", ""))
        userPasswordEditText.setText(sharedPreferences?.getString("password", ""))

    }

}