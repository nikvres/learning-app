package com.example.learningapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.learningapp.R
import com.example.learningapp.adapters.BookAdapter
import com.example.learningapp.api.AppAPI
import com.example.learningapp.interfaces.IBook
import com.example.learningapp.models.Book
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BooksFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val context = inflater.inflate(R.layout.fragment_books, container, false)
        val recyclerView : RecyclerView = context.findViewById(R.id.booksFragment_recyclerView)

        val retrofit = AppAPI().getNewBooksAPI()
        var booksAPI = retrofit.create(IBook::class.java)

        booksAPI.getAllBooks().enqueue(object : Callback<ArrayList<Book>> {
            override fun onResponse(
                call: Call<ArrayList<Book>>,
                response: Response<ArrayList<Book>>
            ) {
                val books = response.body() as ArrayList<Book>
                recyclerView.adapter = BookAdapter(activity!!.applicationContext, books)
            }

            override fun onFailure(call: Call<ArrayList<Book>>, t: Throwable) {
            }

        })


        return context
    }
    
}