package com.example.learningapp.interfaces
import com.example.learningapp.models.Book

import retrofit2.http.GET
import retrofit2.Call
interface IBook {
    @GET("books")
    fun getAllBooks() : Call<ArrayList<Book>>
}