package com.example.learningapp.interfaces

import com.example.learningapp.models.FilmsData
import retrofit2.http.GET
import retrofit2.Call

interface IFilm  {
    @GET("films")
    fun GetAllFIlms() : Call<FilmsData>

}