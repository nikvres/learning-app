package com.example.learningapp.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.learningapp.R
import com.example.learningapp.fragments.FavouriteFragment
import com.example.learningapp.fragments.ProfileFragment
import com.example.learningapp.fragments.SettingsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_navigation)
        val toolbar : androidx.appcompat.widget.Toolbar = findViewById(R.id.toolbar)

        setSupportActionBar(toolbar)
        supportActionBar?.title = "ПРОФИЛЬ"
        supportFragmentManager.beginTransaction().replace(R.id.frame_layout, ProfileFragment()).commit()
        bottomNavigation.selectedItemId = R.id.profile;
        bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.favorites -> {
                    supportActionBar?.title = "ИЗБРАННОЕ"
                    supportFragmentManager.beginTransaction().replace(R.id.frame_layout, FavouriteFragment()).commit()
                }

                R.id.profile -> {
                    supportActionBar?.title = "ПРОФИЛЬ"
                    supportFragmentManager.beginTransaction().replace(R.id.frame_layout, ProfileFragment()).commit()
                }

                R.id.settings -> {
                    supportActionBar?.title = "НАСТРОЙКИ"
                    supportFragmentManager.beginTransaction().replace(R.id.frame_layout, SettingsFragment()).commit()
                }

            }; true

        }
    }
}