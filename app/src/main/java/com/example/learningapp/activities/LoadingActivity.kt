package com.example.learningapp.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import com.example.learningapp.R

class LoadingActivity : AppCompatActivity() {

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        sharedPreferences = getSharedPreferences(USER_TABLE, Context.MODE_PRIVATE)

        val timer = object : CountDownTimer(2000, 1000) {

            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {

                if (sharedPreferences.getBoolean("isLogin", false)) {
                    val intent = Intent(this@LoadingActivity, MainActivity::class.java)
                    startActivity(intent)
                }
                else {

                    val intent = Intent(this@LoadingActivity, OnBoardingActivity::class.java)
                    startActivity(intent)

                }


            }

        }

        timer.start()
    }
}