package com.example.learningapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.learningapp.R

class OnBoardingActivity : AppCompatActivity() {

    lateinit var SignInButton : Button
    lateinit var SignUpTextView : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_on_boarding)

        SignInButton = findViewById(R.id.onBoarding_SignInButton)
        SignUpTextView = findViewById(R.id.onBoarding_SignUpTextView)

        SignInButton.setOnClickListener {

            val intent = Intent(this@OnBoardingActivity, SignInActivity::class.java)
            startActivity(intent)

        }

        SignUpTextView.setOnClickListener {

            val intent = Intent(this@OnBoardingActivity, SignUpActivity::class.java)
            startActivity(intent)

        }
    }
}