package com.example.learningapp.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.example.learningapp.R

class SignInActivity : AppCompatActivity() {

    lateinit var sharedPreferences: SharedPreferences
    lateinit var SignInButton : Button
    lateinit var userEmailEditText: EditText
    lateinit var checkBox: CheckBox
    lateinit var userPasswordEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        SignInButton = findViewById(R.id.signIn_Button)
        userEmailEditText = findViewById(R.id.editTextTextEmailAddress)
        userPasswordEditText = findViewById(R.id.editTextTextPassword)
        checkBox = findViewById(R.id.checkbox)

        sharedPreferences = getSharedPreferences(USER_TABLE, Context.MODE_PRIVATE)
        checkBox.isChecked = sharedPreferences.getBoolean("isLogin", false)

        if (checkBox.isChecked) {
            userEmailEditText.setText(sharedPreferences.getString("email", ""))
            userPasswordEditText.setText(sharedPreferences.getString("password", ""))
        } else {

            userEmailEditText.setText("")
            userPasswordEditText.setText("")

        }


        SignInButton.setOnClickListener {

            if (userEmailEditText.text.toString().isEmpty()
                || userPasswordEditText.text.toString().isEmpty()
            ) {
                Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (sharedPreferences.getString("email", "") == userEmailEditText.text.toString() &&
                sharedPreferences.getString("password", "") == userPasswordEditText.text.toString() ) {

                val intent = Intent(this@SignInActivity, MainActivity::class.java)
                if (checkBox.isChecked) {
                    sharedPreferences.edit().putBoolean("isLogin", true).apply()
                } else {
                    sharedPreferences.edit().putBoolean("isLogin", false).apply()
                }
                startActivity(intent)

            } else {
                Toast.makeText(this, "Неверный логин или пароль", Toast.LENGTH_SHORT).show()
            }

        }

    }
}