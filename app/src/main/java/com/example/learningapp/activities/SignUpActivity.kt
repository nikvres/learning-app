package com.example.learningapp.activities

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.example.learningapp.R
import org.intellij.lang.annotations.RegExp

const val USER_TABLE = "USER_TABLE"

class SignUpActivity : AppCompatActivity() {

    lateinit var signUpButton: Button
    lateinit var haveAccountButton: Button
    lateinit var sharedPreferences: SharedPreferences
    lateinit var userNameEditText: EditText
    lateinit var userEmailEditText: EditText
    lateinit var userPasswordEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        signUpButton = findViewById(R.id.signUpButton)
        haveAccountButton = findViewById(R.id.haveAccountButton)
        userNameEditText = findViewById(R.id.editTextTextNameSignUp)
        userEmailEditText = findViewById(R.id.editTextTextEmailAddressSignUp)
        userPasswordEditText = findViewById(R.id.editTextTextPasswordSignUp)

        sharedPreferences = getSharedPreferences(USER_TABLE, MODE_PRIVATE)

        signUpButton.setOnClickListener {
            checkCredentials()
        }

        haveAccountButton.setOnClickListener {
            val intent = Intent(this@SignUpActivity, OnBoardingActivity::class.java)
            startActivity(intent)
        }

    }

    private fun checkCredentials() {
        if (userNameEditText.text.toString().isEmpty()
            || userEmailEditText.text.toString().isEmpty()
            || userPasswordEditText.text.toString().isEmpty()
        ) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
            return
        }

        if (!checkEmailPattern(userEmailEditText.text.toString())) {
            Toast.makeText(this, "Введите валидную почту", Toast.LENGTH_SHORT).show()
            return
        }

        addUserToSharedPreferences(
            userEmailEditText.text.toString(),
            userPasswordEditText.text.toString(),
            userNameEditText.text.toString()
        )

        val intent = Intent(this@SignUpActivity, SignInActivity::class.java)
        startActivity(intent)

    }

     fun checkEmailPattern(emailString: String): Boolean {

        val emailPattern = "^[a-zA-Z0-9]{1,50}@[a-z]{1,10}\\.[a-z]{1,4}$".toRegex()
        return emailPattern.matches(emailString)
    }

     fun addUserToSharedPreferences(email: String, password: String, name: String) {
        val editor = sharedPreferences.edit()
        editor.putString("email", email)
        editor.putString("password", password)
        editor.putString("name", name)
        editor.apply()
    }

}